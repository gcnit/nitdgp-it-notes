-Importance of synchronization
	-The temporal or time relationship is important in MM presentation.
	-A speaker’s lip movement & words MUST match in the video clip, i.e. they must be SYNCHRONIZED, otherwise, there is a loss of sync.
	
-Spatial Transformation
	-Transformations taking place in space i.e. in screen & audio.
	-It includes transitions on lighting like fading of images or audio, channel mixing, color changes etc.

-Classification of Temporal relationship
	-Object-Stream interaction
		-How object streams interact with each other
		-Intra-media relationship
			-It is interaction between objects of a single stream. 
			-Ex: Animation without sound. If an animation clip is stored at 14 frames/sec, then the clip must be played back at 14 f.p.s. This is intra-media temporal relationship constraint. This is also called serial synchronization.
		-Inter-media relationship
			-It is between objects of two or more streams. 
			-Ex: Video clip with sound; also called parallel synchronization.

		-User-interaction based temporal relation
			-In an interactive MM system, it is the interaction between user & the system.
			-The time taken by the system to respond to the user is called response time which should be consistent & as small as possible for wither stand alone or networked interactive MM system. This is also called event synchronization.

		-Transportation of a Multimedia stream on a networked system 
			-Node N1 is transmitting MM info thru a communication link (N1, N2) to node N2.
			-Each node has a buffer of temporary storage of data i.e. MM objects are retrieved from storage or received from a live source (say video camera).
			-MM info is formatted into packets & transmitted in communication link in form of streams or packets.
			-Two object streams are shown.
				-Stream A has m objects.
				Stream (A):= (A1, A2,……., Am, Am+1…): = Ψ(Ap)
				-Stream B has n objects
				Stream (B):= (B1, B2,……., Bn,  Bn+1…): = Ψ(Bq)
				Where Ap is any object in stream (A) & Bq is any object in stream (B) & Ψ(Ap) is the sequence of type A objects.

	-Based on Media levity
		-Live interaction
			-Live presentation demanding minimum end-to-end delay.
			-A MM presentation requires composition of MM streams. 
			-It has two aspects 
				-spatial (placement of MM object at space at any point of time), 
				-temporal (takes care of synchronization between object stream which are of two types: point & stream synchronization). 
			-Point synchronization is used when a single object is in one stream must be in synchronization with an object in other stream at one point of time. 
			-Stream synchronization is done continuously over a period of time. Stream sync is also called continuous sync.
		-Stored interaction
			-They are retrieved from secondary storage.
			-Ex: VOD (Video-on-demand).
		-Mixed
			-Combination of live & stored presentation like collaborative conference.

-Media Synchronization
	-Asynchronous
		-No well defined timing relationships exists between objects of a single stream or between objects of different streams.
	-Synchronous
		-Well defined temporal relationships between objects of different streams.
		-Ex: Video with sound.
	-Isochronous
		-Well defined time relations between objects of same stream.
		-Ex: Sound

-Specifications of temporal relationships
	-The temporal relationships between various objects in MM presentation MUST BE CLEARLY SPECIFIED either using relative or absolute temporal specifications.
	-Relative time specifications
		-Many MM presentations do NOT need EXACT specifications.
		-For ex. for an educational courseware called “Introduction to MM” a welcome screen appears & then an introductory voice message.
		So, relative specs. are more important that absolute specs here.
		The voice message must play isochronously regardless of computer speed.
		-There are seven fundamental and six inverse Relative Temporal Relationships (RTRs) between two objects, totaling 13 RTRs.
		-Relative Temporal Relationships (RTRs)
			-The inverse transformation is only one type of transformation, the others being scaling (used for fast forward) & shifting (used for slow motion). Inverting is used for Rewind.
			-Temporal transformations are actually used to map the stored temporal relationships for a real time play-out.
	-Absolute time specifications
		-There are three types
			-Instant based specification
				-It is appropriate when there is a need to state that play-out begins exactly at time t1 & finishes at t3.
				-Values of this instant may be specified either w.r.t. reference time  i.e. t0=0:00 t1 = 10secs, t2 = 20 secs, etc. or in terms of local time i.e. t1=05:45 hrs
			-Interval based specification
				-Gives the interval for play out.
				-The starting time t1 & play-out interval Tplay are specified.
				-It is easy to map a fixed interval spec. to a starting & finishing instant based specification.
				-It is more important when inexact spec. is used.
			-Inexact timing specification
				-It may sometimes be used in some applications to specify play-out period.
				-Ex: It will be appropriate to specify “music should start soon after still image & as soon as the music finishes, image must fade out.”
				-A lower & upper bound on the play-out period is provided.
