SYNCHRONIZATION ACCURACY SPECIFICATIONS (SAS)

-Synchronization parameters
	-Synchronization between different object streams is important for achieving desired quality of multimedia (MM) presentation.
	-Level of inter-stream and intra-stream synchronization is specified in terms of synchronization Accuracy Specifications (SAS) factors:
		-Delay
		-Jitter
		-Skew
		-Error rates
	-The allowable range of SAS depends on the application

-QoS – Quality of Service
	-Networked MM systems demand guaranteed performance from end-to-end.
	-All system components
		-CPU, disk system, system bus, communications protocols, communications media, system buffers and encoding techniques play important role in delivery of QoS.
	-Quality of Service represents the set of those quantitative and qualitative characteristics of distributed Multimedia system necessary to achieve the required functionality of an application.
-Acceptable values of SAS Factors
	-They are specified as range of values
	-They depend on QoS requirement of system.
	-The level of performance on 486-based computer is not acceptable on a pentium-based computer.
-Delay factors for stored & captured MM objects 
	-Some delay factors occur only in retrieving stored objects & some occur only for objects captured in real time.
	-Check slide(Page-7)
-