#O(n^2)
def lps(arr):
    n = len(arr)
    LP = [[0 for x in range(n)] for x in range(n)]
    for i in range(0,n):
        LP[i][i] = 1
    for gap in range(1,n):
        for i in range(0,n-gap):
            j=i+gap
            if (arr[i] == arr[j]) and (gap == 1):
                LP[i][j]=2
            elif arr[i] == arr[j]:
                LP[i][j]=LP[i+1][j-1]+2
            else:
                LP[i][j]= max(LP[i][j-1], LP[i+1][j])
    sub = '';
    x = 0;
    y = n-1;
    while x < y:
        if arr[x] == arr[y]:
            sub += arr[x]
            x+=1;
            y-=1;
        elif LP[x][y-1] > LP[x+1][y]:
            y-=1
        else:
            x+=1
    subrev = sub[::-1]
    if x == y:
        sub += arr[x]
    sub += subrev
    return sub
arr = raw_input()
print(lps(arr))
