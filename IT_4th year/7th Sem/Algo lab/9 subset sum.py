#Polynomial in n and exponential in no of bits used to represent the sum
def subsetSum(a, s):
	c = 0.01
	S = [0]
	n = len(a)
	for i in range(0,n):
		T = [a[i]+j for j in S]
		U = list(set(S) | set(T))
		U.sort()
		S = []
		y = U[0]
		S.append(y)
		for z in U:
			if z-y > (c*s)/n and z <= s:
				y = z
				S.append(z)

	n = len(S)
	for i in range(0,n):
		if S[i] >= (1-c)*s and S[i] <=s:
			print "yes"
			return
	print "no"
a = [1,2,3,4]
subsetSum(a, 10)
