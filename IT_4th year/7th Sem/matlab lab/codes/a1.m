clc;
a=[1,2,3,4,6;4,3,2,0,8;0,2,2,1,10;4,6,8,6,24];
b = zeros(3,4);
supply = a(:,end);
demand = a(end,:);
a = a(1:end-1, 1:end-1);
sizeA = size(a);
cost = 0;
while(min(min(a))~=inf)
    index = find(a==min(min(a)),1);
    row = mod(index-1, sizeA(1))+1;
    col =  floor((index-1)/sizeA(1))+1;
    sub = min(supply(row), demand(col));
    supply(row) = supply(row)-sub;
    demand(col) = demand(col)-sub;
    cost = cost + a(row, col)*sub;
    if(supply(row)==0)
        a(row,:)=inf;
    end
    if(demand(col)==0)
        a(:,col)=inf;
    end
    b(row,col) = sub;
end
disp(cost);