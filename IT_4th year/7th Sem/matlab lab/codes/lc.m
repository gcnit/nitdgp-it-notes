function lc(a, supply, demand)
    b = zeros(3,3);
    cost = 0;
    while(min(min(a))~=inf)
        index = find(a==min(min(a)),1);
        row = mod(index-1, size(a,1))+1;
        col =  floor((index-1)/size(a,1))+1;
        sub = min(supply(row), demand(col));
        supply(row) = supply(row)-sub;
        demand(col) = demand(col)-sub;
        cost = cost + a(row, col)*sub;
        if(supply(row)==0)
            a(row,:)=inf;
        end
        if(demand(col)==0)
            a(:,col)=inf;
        end
        b(row,col) = sub;
    end
    disp('By Least Cost Method');
    disp(b);
    disp(cost);
end