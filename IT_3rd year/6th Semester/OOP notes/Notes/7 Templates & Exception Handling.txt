Templates
	-Templates are used to define generic classes and functions
	-It supports generic programming
	-Generic class: When an object of specific type is defined for actual use, the template definition for that class is substituted with the required data type
	-There are two types of templates:
		-Class template
		-Function template
Exception Handling
	-There are two common types of errors
		-Logic error: Caused due to poor understanding of the problem
		-Syntactic error: Caused due to poor understanding of the language
	-Both types of error can be solved by proper debugging and testing
	-Some particular type of errors still exists
	-They are known as exceptions
	-They are
		-runtime errors
		-divide by 0
		-access to array out of bound
		-running out of memory or disk space etc
	-If the program encounters an exceptional condition, it has to be identified and dealt with
	-This is known as exception handling
	-To handle exception these steps are followed:
		-Find the problem (Hit the exception)
		-Inform that an error has occurred (Throw the exception)
		-Receive the error information (catch the exception)
		-Take corrective actions (Handle the exception)		
	-Exception Handling Mechanism
		-try{
		--------
		throw exception;
		}
		catch(type argument) {----}
	
	-Throwing Mechanism
		-throw(exception);
		-throw exception;
		-throw;
		-Once the exception is thrown to catch block, control cannot return to the throw point
	-Catching Mechanism
		-catch(type argument){ }
		-Type indicates the type of exception that catch block handles
		-Catch catches an exception whose type matches with the type of catch argument
		-If the type mismatch occurs, catch block is simply skipped and abnormal termination of the program occurs
		-After handling the exception, the control goes to the statement immediately following the catch block
		-Multiple catch blocks: 
			-try{  }
			catch(type1 argument){ }
			catch(type2 argument) {}
			-Each catch is checked in order
			-When two catch matches, the first will be considered
		-Catch all exceptions
			-To catch all statements from one catch block we have to use catch(…){  }
		-Rethrowing an exception
			-A handler may decide to rethrow the exception caught without processing it
			-throw;
			-This throws the current expression to the next try/catch sequence
	-Specifying Exception
		-It is possible to restrict a function to throw only certain specified exceptions
		-This can be done by adding throw list to function definition
		-Throwing any exception out of this list will cause abnormal termination of the problem
		-Type function(arg_list) throw(type-list){ }