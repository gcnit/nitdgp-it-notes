#include<iostream>


using namespace std;
class Account
{
    char *name;
    long ac_num;
    char ac_type;
    double bal;

public:

    void init_data(char *n,long num,char type,double b)
    {
        name=n;
        ac_num=num;
        ac_type=type;
        bal=b;
    }

    void deposit(double amt)
    {
        bal=bal+amt;
        cout<<"Rs "<<amt<<" deposited in account "<<ac_num<<endl;
        cout<< "Current account balance for account "<<ac_num<<" is Rs "<<bal<<endl<<endl;
    }

    void withdraw(double amt)
    {

        if(amt>=bal-1000)
            cout<<"Insufficient funds to withdraw Rs "<<amt<<" from account "<<ac_num<<endl<<endl;
        else
        {
            bal=bal-amt;
            cout<<"Rs "<<amt<<" withdrawn from account "<<ac_num<<endl;
            cout<< "Current account balance for account "<<ac_num<<" is Rs "<<bal<<endl<<endl;

        }
    }

    void display()
    {
        cout<<"-------Account Details-------"<<endl;
        cout<<"Account Holder's Name: "<<name<<endl;
        cout<<"Account Number: "<<ac_num<<endl;
        cout<<"Account Type: "<<ac_type<<endl;
        cout<< "Current balance: Rs "<<bal<<endl<<endl;

    }

    long get_acc_num()
    {

        return this->ac_num;
    }

};

int main()
{
    Account acc[50];
    int i=-1;
    bool s=true;
    while(s){
    int ch;
    cout<<endl<<"-------Menu---------"<<endl;
    cout<<"1. Create a new account"<<endl;
    cout<<"2. Deposit Money in an account"<<endl;
    cout<<"3. Withdraw money from an account"<<endl;
    cout<<"4. Display Account details"<<endl;
    cout<<"5. Exit"<<endl<<endl;
    cin>>ch;
    switch(ch)
    {
        char n[60];
            long num;
            char t;
            double bal;
            int flag;
            double amt;
    case 1:
            i++;
            cout<<"Enter name of account holder: ";
            cin>>n;
            cout<<"Enter account number: ";
            cin>>num;
            cout<<"Enter Account type: ";
            cin>>t;
            cout<<"Enter current balance: ";
            cin>>bal;
            acc[i].init_data(n,num, t, bal);
            acc[i].display();
            break;

    case 2:
            flag=0;
            cout<<"Enter account number: ";
            cin>>num;
            cout<<"Enter amount to deposit: ";
            cin>>amt;

            for(int k=0;k<=i;k++)
            {
                if(acc[k].get_acc_num()==num)
                {
                    acc[k].deposit(amt);
                    flag=1;
                }
            }
            if(flag==0)
                cout<<"Invalid account number"<<endl;
            break;

    case 3: cout<<"Enter account number: ";
            cin>>num;
            cout<<"Enter amount to withdraw: ";
            cin>>amt;
            flag=0;
            for(int k=0;k<=i;k++)
            {
                if(acc[k].get_acc_num()==num)
                {
                    acc[k].withdraw(amt);
                    flag=1;
                }
            }
            if(flag==0)
                cout<<"Invalid account number"<<endl;
            break;

    case 4:
            cout<<"Enter account number: ";
            cin>>num;
            flag=0;
            for(int k=0;k<=i;k++)
            {
                if(acc[k].get_acc_num()==num)
                {
                    acc[k].display();
                    flag=1;
                }
            }
            if(flag==0)
                cout<<"Invalid account number"<<endl;
            break;

    case 5: s=false;
            break;

    default: cout<<"Enter a valid choice"<<endl;


    }
    }




    return 0;
}
