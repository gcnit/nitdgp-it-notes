//0000000000000000000000000000//
//%%%%   Archit Srivastava %%%//
//        NIT Durgapur        //
//0000000000000000000000000000//
#include<bits/stdc++.h>
using namespace std;
#define PB push_back
#define MP make_pair
#define clr(x) x.clear()
#define sz(x) ((int)(x).size())
#define F first
#define S second
#define REP(i,a,b) for(i=a;i<b;i++)
#define rep(i,b) for(i=0;i<b;i++)
#define rep1(i,b) for(i=1;i<=b;i++)
#define mod 1000000007
#define pi(n) printf("%d",n)
#define pin(n) printf("%d\n",n)
#define piw(n) printf("%d ",n)
#define pll(n) printf("%lld",n)
#define plln(n) printf("%lld\n",n)
#define pllw(n) printf("%lld ",n)
#define sll(n) scanf("%lld",&n)
#define ss(s) scanf("%s",s)
#define ps(s) printf("%s",s)
#define psn(s) printf("%s\n",s)
#define psw(s) printf("%s ",s)
#define si(n) scanf("%d",&n)
#define pn printf("\n")
#define pw printf(" ")
#define PI (3.141592653589793)

//inline int ri()
//{
//    int n=0;char c;
//    while(1)
//    {
//        c=getchar();//_unlocked();
//        if(c=='\n'||c==' '||c==EOF)break;
//        n=(n<<3) + (n<<1) +c-48;
//    }
//    return n;
//}
inline long long rll()
{
    long long n=0;
    char c;
    while(1)
    {
        c=getchar();//_unlocked();
        if(c=='\n'||c==' '||c==EOF)break;
        n=(n<<3) + (n<<1) + c - 48;
    }
    return n;
}

typedef pair<int,int> PII;
typedef vector<PII> VPII;
typedef vector<int> VI;
typedef vector<VI> VVI;
typedef long long LL;

LL mulmod(LL a, LL b, LL c)
{
    LL x=0, y=a%c;
    while(b>0)
    {
        if(b&1)x+=y;
        if(x>c)x%=c;
        y*=2;
        if(y>c)y%=c;
        b>>=1;
    }
    return x%c;
}


LL modexp(LL a, LL p, LL m)
{
    LL ans=1,mul=a; if(!p)return (LL)(1);
    while(p!=0)
    {
        if(p&1)ans=mulmod(ans,mul,m);
        if(ans>m)ans%=m;
        mul=mulmod(mul,mul,m);
        if(mul>m)mul%=m;
        p>>=1;
    }
    return ans;
}

int gcd(int a,int b)
{
    if(a<b)return gcd(b,a);
    if(!b)return a;
    return gcd(b,a%b);
}

int nextpoweroftwo(int a)
{
    int p=1;
    while(p<=a)p<<=1;
    return p;
}

#define LEN 10000

int main()
{
    char a[LEN], b[LEN];
    scanf("%s", a);
    scanf("%s", b);
    if(strlen(b)>strlen(a))swap(a,b);
    int i,j,l1=strlen(a),l2=strlen(b);
    for(i=0;i<=l1-l2;i++)
    {
        for(j=0;j<l2;j++)if(a[i+j]!=b[j])break;
        if(j==l2)printf("String matched at %d\n", i);
    }

}
