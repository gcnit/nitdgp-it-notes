mvi b,0Ah
lxi h,0010h
mvi d,00h
mov m,d
inx h
mvi e,01h
mov m,e
inx h
loop: mov a,e
      add d
      mov m,a
      inx h
      mov d,e
      mov e,a
      dcr b
      jnz loop
hlt